<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<link href="./css/default.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="css/navbar.css">
<script src="jquery-1.11.1.min.js"></script>
<script src="navbar.js"></script>
<script>
$(document).ready(function(){
    $("#hide_org").click(function(){
        $("#org_abun").hide();
    });
    $("#show_org").click(function(){
        $("#org_abun").show();
    });
      $("#hide_name").click(function(){
	              $("#name_abun").hide();
		          });
        $("#show_name").click(function(){
		        $("#name_abun").show();
	});
      $("#hide_func").click(function(){
	              $("#func_abun").hide();
		          });
        $("#show_func").click(function(){
		        $("#func_abun").show();
	});
     $("#hide_mview").click(function(){
	                           $("#mview_frame").hide();
				                             });
            $("#show_mview").click(function(){
		                            $("#mview_frame").show();
	    });
     $("#hide_phylo").click(function(){
                                   $("#phylo").hide();
                                                             });
            $("#show_phylo").click(function(){
                                            $("#phylo").show();
                                                                        });
});
</script>
<script type="text/javascript">
$(document).ready(function() {
        // Animate loader off screen
                 $(".se-pre-con").fadeOut("slow");;
                                         });
                                         </script>
<title>BioSurfDB</title>
</head>
<body>
<div class="se-pre-con"><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p>Computing BLAST...<br>Please wait...</div>
<div class="body">
<?php include "header_and_left_bar.php"?>
<p>&nbsp;</p>
<div class="title">Submit BLAST</div>

<?php


// information check

function died($error) {
echo "<p>&nbsp;</p>$error";
echo "<p>Please go back and fix these errors.</p><p>&nbsp;</p>";
echo "<FORM><INPUT Type=\"button\" VALUE=\"Back to previous page\" onClick=\"history.go(-1);return true;\"></FORM><p>&nbsp;</p><p>&nbsp;</p>";
die();
}

if(!isset($_POST['sequence']) ||
!isset($_POST['target']) ||
!isset($_POST['program']) ||
!isset($_POST['format']) ||
!isset($_POST['evalue']) ||
!isset($_POST['word_size']) 
) {
	died("We are very sorry, but there were error(s) found with the form you submitted.");
}
		
$sequence = $_POST['sequence'];
$sequence = preg_replace('/[^A-Za-z0-9\-\s\>]/', '', $sequence);
$target = $_POST['target'];
$target = explode(" ",$target);
$target_type = end($target);
$target = $target[0];
$program = $_POST['program'];
$format = $_POST['format'];
$evalue = $_POST['evalue']; 
$word_size = $_POST['word_size'];
#$email = $_POST['email'];

if ($word_size == 'default')
{
if ($program == 'blastn')
{
	$word_size = 11;
}
else
{
	$word_size = 3;
}
}

// check word size

if(!is_numeric($word_size))
{
 $error_message .= '<p>The word size must be an integer</p>';
}
if( ( ($program == 'blastp') or ($program == 'tblastn') or ($program == 'blastx') or ($program == 'tblastx') ) and ( $word_size < 2 ) )
{
	$error_message .= '<p>The minimum word size for Protein BLAST is 2 residues.</p>';
}
if( ($program == 'blastn') and ( $word_size < 4 ) )
{
	$error_message .= '<p>The minimum word size for BLASTn is 4 nucleotides.</p>';
}

if($_POST['jobtitle'] != '')
{
$jobtitle = $_POST['jobtitle'];
$jobtitle = str_replace(' ', '_', $jobtitle);
$jobtitle = preg_replace("/[^A-Za-z0-9\-\_]/", '', $jobtitle);
$jobtitle = preg_replace('/\s+/', '', $jobtitle);
}
else
{
$jobtitle = time();
}

// check program and database compatibility

if ($program === 'Select')
{
$error_message .= '<p>Select one type of BLAST alignment</p>';
}

if ( ($program === 'blastn' and $target_type !== '(nucl)') or
	 ($program === 'blastp' and $target_type !== '(prot)') or
	 ($program === 'tblastn' and $target_type !== '(nucl)') or
	 ($program === 'tblastx' and $target_type !== '(nucl)') or
	 ($program === 'blastx' and $target_type !== '(prot)') )
{
$error_message .= '<p>The combination of program and target database you have selected are not possible.</p>';
}

//check email


/*$email_exp = '/^[A-Za-z0-9._%-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$/';
if(!preg_match($email_exp,$email)) {
$error_message .= '<p>The Email Address you entered does not appear to be valid.<p/>';
}*/

// check FASTA sequence

if (($program === 'blastn') or ($program === 'blastx') or ($program === 'tblastx'))
{
	$string_exp = "/^[>]+[A-Za-z0-9._%-|\s]+[\n]+[ATCGUatcgu]/";            
}
else 
{
	$string_exp = "/^[>]+[A-Za-z0-9._%-|\s]+[\n]+[A-Za-z]/";
}

if(!preg_match($string_exp,$sequence)) {
														
$error_message .= "<p>The sequence your entered does not match with the type of blast program you have selected or does not appear to be in the FASTA format.</p>
									 <p>$sequence</p>
									 <p> It should follow the following format and avoid using ''strange'' characters:</p><p> >identifiers(s)</p><p>ATCGATCGCAACTGTGTCAGT</p>";
}

// check evalue

if(!is_numeric($evalue))
{
$error_message .= '<p>The Expect threshold must be a number</p>';
}

// metaphlan check

$runmeta = $_POST['runmeta'];

if($runmeta === 'yes')
{
	if ( (!isset($_POST['evalue2'])) ||
		(!isset($_POST['taxlev'])) ||
		(!isset($_POST['top'])) ||
		(!isset($_POST['scale'])) || 
		(!isset($_POST['minv'])) ){ $error .= '<p>Some values of the phylogenetic analysis are missing.</p>'; }
	else
	{
		$evalue2 = $_POST['evalue2'];
		$taxlev	 = $_POST['taxlev'];
		$top 	 = $_POST['top'];
		$scale 	 = $_POST['scale'];
		$minv 	 = $_POST['minv'];
	}
}

if(isset($error_message)) {
	died($error_message);
}

// end of information check
// start of query processing

$html = 0;
       if ( $format === '666' ) {
            $html   = 1;
            $format = 0;
        }
$abundance=0;
        if ( $format === '777' ){
                $abundance=1;
		$format = 7;
        }

# create a cron to update de databases daily
        /*if ( $target === 'database_genes' ) {
            system("/usr/bin/perl /kdbio/home/biosurfdb/public_html/update_gene_database.pl");
        }
        elseif ( $target === 'database_proteins' ) {
            system("/usr/bin/perl /kdbio/home/biosurfdb/public_html/update_protein_database.pl");
	}*/ 

        $now_string = time();


	

 $blast_path = "/usr/bin/";
 $db_path    = "/kdbio/home/biosurfdb/blast-lib/";

 chop($sequence);


 $delete_fasta = 1;
 $query_file = "/kdbio/home/biosurfdb/public_html/tmp/" . $jobtitle . ".fasta";
 $result_file = $jobtitle . '.txt';


$handle = fopen($query_file, 'w');

fwrite($handle, $sequence);

fclose($handle);

# METAPHLAN TAXONOMY 

        if ($runmeta === 'yes')
        {

        $meta_dir = "/kdbio/home/biosurfdb/public_html/metaphlan/";

        $metaphlan = "/usr/bin/python /kdbio/home/jso/microEgo/sw/metaphlan/metaphlan.py";

        $heatmap = "/usr/bin/python /kdbio/home/jso/microEgo/sw/metaphlan/plotting_scripts/metaphlan_hclust_heatmap.py";

        $meta_bd = "/kdbio/home/jso/microEgo/sw/metaphlan/blastdb/mpa";

        $meta_cmd = "$metaphlan $query_file --evalue $evalue2 --tax_lev $taxlev --input_type multifasta -t rel_ab --blastdb $meta_bd --blastout $meta_dir$jobtitle.blastout.tab -o $meta_dir$jobtitle.rel_ab.txt --nproc 8 2> $metadir"."metaphlan.error";

        $merge_cmd = "/usr/bin/python /kdbio/home/jso/microEgo/sw/metaphlan/utils/merge_metaphlan_tables.py $meta_dir$jobtitle.rel_ab.txt > $meta_dir$jobtitle-merged.txt 2> $metadir"."merge.error";

        $heat_cmd = "$heatmap -c bbcry --top $top -s $scale --minv $minv --in $meta_dir$jobtitle-merged.txt --out $meta_dir$jobtitle.png 2> $metadir"."heatmap.error";

        system($meta_cmd); 

        system($merge_cmd);

        system($heat_cmd);

        $tax_link = "http://aleph.inesc-id.pt/~biosurfdb/metaphlan/$jobtitle.png";

        }

        if ( $program === 'blastp' || $program === 'blastx' ) {
            $suffix = "-protein";
        }
        else {
            $suffix = "-nucleotide";
        }

        $dir = "/kdbio/home/biosurfdb/public_html/tmp/" ;

chdir($dir);


        $command =
            $blast_path
          . $program
          . " -query "
          . $query_file . " -db "
          . $db_path
          . $target
          . $suffix
          . " -out "
          . $result_file
          . " -evalue "
          . $evalue
          . " -word_size "
          . $word_size
          . " -outfmt "
          . $format
          . " -num_threads 8 2> blast_error.log";

	
        system($command); 

		$query_count = $nohits = $matches = 0;
if ($format === 0) {
                $handle = fopen("$result_file", 'r');
                if ($handle) {
                    while (($line = fgets($handle)) !== false) {
			if (preg_match("/Query= /", $line)) {
                                $query_count++;
                        }
			if (preg_match("/No hits found/", $line)) {
				$nohits++;
			}
                	}
		}
                fclose($handle);
$matches=$query_count-$nohits;	
}
elseif ($format === 7) {
	$handle = fopen("$result_file", 'r');
                if ($handle) {
                    while (($line = fgets($handle)) !== false) {
                        if (preg_match("/^#/", $line)) {
				if (preg_match("/^# Query: /", $line)) {
					$query_count++;
				}
                        }
                        else  {
                                $matches++;
                        }
                        }
                }
                fclose($handle);
}

        if ($delete_fasta) {
            system("rm -rf $query_file");
        }


	$result_link = 'http://kdbio.inesc-id.pt/~biosurfdb/tmp/' . $result_file;

	echo"<p>Congratulations! Your BLAST job has been completed.</p><p> Processed $query_count sequences and got $matches hits.</p>";
	echo"<p><FORM><INPUT Type=\"button\" VALUE=\"Back to previous page\" onClick=\"history.go(-1);return true;\"></FORM></p>";

	if($runmeta === 'yes')
	{
		echo"   <div class=\"list\">
                                <table class=\"list\" style=\"width:30%\">
                                <tr><th style=\"width:30%;\"> Phylogenetic Analysis </th>
                          <td><button id=\"show_phylo\">Show </button></td>
                          <td><button id=\"hide_phylo\">Hide </button></td>
                          <td><form  action=\"download_blast.php\">
                          <input type=\"hidden\" name=\"filename\" value=\"$jobtitle\">
                          <input type=\"hidden\" name=\"dir\" value=\"$dir\">
                          <input type=\"submit\" value=\"Download \">
                          </form></td></tr></table></div>
                          <iframe id=\"phylo\" src=\"./metaphlan/$jobtitle.png\" width=\"80%\" height=\"1000\"></iframe>";
	}

	echo"<div class=\"list\">
		<table class=\"list\" style=\"width:30%\">
		<tr><th style=\"width:30%;\">BLAST Alignment</th>";

	if (($html) and ($matches !== 0)) {
		echo"<td><button id=\"show_mview\">Show</button></td>
			<td><button id=\"hide_mview\">Hide</button></td>";
	}
		echo"<td><form action=\"download_blast.php\">
		<input type=\"hidden\" name=\"filename\" value=\"$result_file\">
		<input type=\"hidden\" name=\"dir\" value=\"$dir\">
		<input type=\"submit\" value=\"Download\">
		</form></td></tr></table></div>
		";

if ($matches !== 0) {

          # BEGIN CREATE ABUNDANCE GRAPHIC

	$bin = "/kdbio/home/biosurfdb/bin/";

	

          if ($abundance)
          {

                # ORGANISMS

                  $parsed_file = $result_file . '.counts';
                  $parse_blast_cmd = "cat $result_file | ".$bin."blast_besthit.pl | ".$bin."tabular-blast-organism-counter.pl > ".$dir.$parsed_file ." 2> parse_log";
                  $generate_graphics = "R --no-restore --no-save --args $parsed_file $evalue < ".$bin."alpha_barplot.R 2> barplot_eror";
                  exec($parse_blast_cmd) ;
		  exec($generate_graphics) ;
		  system("mv -f ./$jobtitle.pdf ./$jobtitle-organism-abundance.pdf");
                  $image = "./$jobtitle-organism-abundance.pdf";
		  $result_link = $result_link . "\nhttp://kdbio.inesc-id.pt/~biosurfdb/tmp/" .$image;

		  echo"   <div class=\"list\">
			  <table class=\"list\" style=\"width:30%\">
			  <tr><th style=\"width:30%;\"> Organism Abundance </th>
			  <td><button id=\"show_org\">Show</button></td>
			  <td><button id=\"hide_org\">Hide</button></td>
			  <td><form action=\"download_blast.php\">
			  <input type=\"hidden\" name=\"filename\" value=\"$image\">
			  <input type=\"hidden\" name=\"dir\" value=\"$dir\">
			  <input type=\"submit\" value=\"Download\">
			  </form></td></tr></table></div>
			  <iframe id=\"org_abun\" src=\"./tmp/$image\" width=\"80%\" height=\"1000\"></iframe>";
		  
		  # NAMES
		  
		  $parse_blast_cmd = "cat $result_file | ".$bin."blast_besthit.pl | ".$bin."tabular-blast-name-counter.pl > ".$dir.$parsed_file . " 2> parse_log";
                  $generate_graphics = "R --no-restore --no-save --args $parsed_file $evalue < ".$bin."create_barplot.R";
                  system($parse_blast_cmd) ;
		  exec($generate_graphics) ;
		  system("mv -f ./$jobtitle.pdf ./$jobtitle-name-abundance.pdf");
                  $image = "./$jobtitle-name-abundance.pdf";
		  $result_link = $result_link . "\nhttp://kdbio.inesc-id.pt/~biosurfdb/tmp/" .$image;

		  echo"   <div class=\"list\">
			  <table class=\"list\" style=\"width:30%\">
			  <tr><th style=\"width:30%;\"> Name Abundance </th>
			  <td><button id=\"show_name\">Show </button></td>
                          <td><button id=\"hide_name\">Hide </button></td>
                          <td><form  action=\"download_blast.php\">
                          <input type=\"hidden\" name=\"filename\" value=\"$image\">
                          <input type=\"hidden\" name=\"dir\" value=\"$dir\">
                          <input type=\"submit\" value=\"Download\">
			  </form></td></tr></table></div>
			  <iframe id=\"name_abun\" src=\"./tmp/$image\" width=\"80%\" height=\"1000\"></iframe>";


                if ($program === 'blastp' or $program === 'blastx')
                {
                        $parse_blast_cmd = "cat $result_file | ".$bin."blast_besthit.pl | ".$bin."tabular-blastp-to-functional.pl > ".$dir.$parsed_file ." 2> parse_log";
                        $generate_graphics = "R --no-restore --no-save --args $parsed_file $evalue < ".$bin."function_barplot.R";
                        system($parse_blast_cmd) ;
			exec($generate_graphics) ;
			system("mv -f ./$jobtitle.pdf ./$jobtitle-functional-abundance.pdf");
			system("rm -rf ./$parsed_file");
                        $image = "./$jobtitle-functional-abundance.pdf";
			$result_link = $result_link . "\nhttp://kdbio.inesc-id.pt/~biosurfdb/tmp/" .$image;
			
			echo"   <div class=\"list\">
				<table class=\"list\" style=\"width:30%\">
				<tr><th style=\"width:30%;\"> Funcional Abundance </th>
			  <td><button id=\"show_func\">Show </button></td>
                          <td><button id=\"hide_func\">Hide </button></td>
                          <td><form  action=\"download_blast.php\">
                          <input type=\"hidden\" name=\"filename\" value=\"$image\">
                          <input type=\"hidden\" name=\"dir\" value=\"$dir\">
                          <input type=\"submit\" value=\"Download \">
			  </form></td></tr></table></div>
			  <iframe id=\"func_abun\" src=\"./tmp/$image\" width=\"80%\" height=\"1000\"></iframe>";
                }
          }


          # BEGIN CREATE HTML FILE

if ($html) {
             $input = "/kdbio/home/biosurfdb/public_html/tmp/" . $result_file;

             $exp = "/Query= /";
             $final  = $input . '.html';
             $reg    = "/No hits found/";
             $filter = 0;
             $mview  = "/kdbio/home/jso/microEgo/sw/mview-1.56/bin/mview";
             $mview_header = "/kdbio/home/biosurfdb/public_html/mview-header.html";

		$handle = fopen("$final", 'w');
                $handle2 = fopen("$mview_header", 'r'); 
		if ($handle2) {
		    while (($line = fgets($handle2)) !== false) {
			fwrite($handle, "$line");
			  }
			fclose($handle2);
		}
		fclose($handle);


            $handle = fopen("$input",'r' );
$num_lines=count(file($input));
	$num=0;
		if ($handle) {
			while (($line = fgets($handle)) !== false) {
				$num++;
				chop($line);
                                if ( ($num === $num_lines) and ( $filter === 0 ) ) {
                                        $result = fopen("$file", 'w');
                                        fwrite($result, "$header$output");
                                        fclose($result);
                                        $command = "$mview -in blast -ruler on -html body -css on -coloring identity -dna -hsp ranked -top 5 -range all $file >> $final 2> mview_log";
                                        exec($command);
                                        #system("rm -rf $file");
					break;
                                }
				if ( substr ( $line, 0, 7) === 'Query= ') {
                                        if ((isset($file)) and ($filter === 0 )) {
                                                $result = fopen("$file", 'w');
                                                fwrite($result, "$header$output");
                                                fclose($result);
                                                $command = "$mview -in blast -ruler on -html body -css on -coloring identity -dna -hsp ranked -top 5 -range all $file >> $final";
                                                system($command);
                                                #system("rm -rf $file");
                                        }
                                        $filter = 0;
                                        $query    = ltrim( $line, "Query= ");
                                        $query=preg_replace("/\s/","_",$query);
                                        $query=preg_replace("/[^0-9a-zA-Z\-\.]/","",$query);
                                        chop($query);
                                        $file   = $query . '.dat';
                                        $output = $line;
                                        continue;
                                }
				if ( !isset($file)) {
					if (isset($header)) {
						$header = "$header$line";
					}
					else {
						$header=$line;
					}
				}
                		if ( isset($file) )
                		{
                    			if ( preg_match("$reg", $line)) # if no hits are found
		    			{
                        			#$filter = 1;
                    			}
		    			else { 
			    			$output = "$output$line" ;
		    			}
				}
			}
		}
		fclose($handle);

		echo"<iframe id=\"mview_frame\" src=\"./tmp/$jobtitle.txt.html\" width=\"80%\" height=\"1000\"></iframe>";

} # if html

} # if matches !== 0
?>

<p>&nbsp;</p>
</body>
</html>
