<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>BioSurfDB</title>
<link href="./css/default.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="css/navbar.css">
<script src="jquery-1.11.1.min.js"></script>
<script src="navbar.js"></script>
<script>
$(document).ready(function(){
    $("form").submit(function(){
            $(".submited").show();
                                       });
});
</script>
</head>
<body>
<div class="body">
<div class="submited"><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p>Submiting form...<br>Please wait...</div>
<?php include "header_and_left_bar.php"?>
<p></p>
<div class="title"> BLAST Search: Please fill the fields below</div>
<div class="right">
<a href="tutorial.php#example1" title="Help">Help <img style="border:0" src="images/information.gif" alt="help icon"></a>
<a href="blast_form_filled.php" title="Sample Form">Sample Data <img style="border:0" src="images/sampledata.gif" alt="sample icon"></a>
</div>
<form name="blast_form" method="post" action="run_blast.php" id="form_blast">
<div class="list">
<fieldset><legend><strong>Insert Nucleotide or Protein Query Sequence in FASTA format</strong></legend>
<table class="left" >
<tr><td style="vertical-align: top;"><textarea id="sequence" name="sequence" maxlength="10000" cols="70" rows="6" onchange="if (this.value.length==this.getAttribute('maxlength')) alert('You have exceeded the maximum sequence length of 10.000 bp. If you submit, sequence will be truncated.')" autofocus></textarea></td>
<td> <p class=shade> (Maximum sequence length of 10.000) </p></td></tr>
<tr><td colspan="2" style="text-align:center">
</td></tr></table>
</fieldset>
<fieldset>
<legend><strong> Select Program </strong></legend>
<table>
<tr><td>
<div>
<select name="program" id="program" class="">
<option value="Select">--Select the type of BLAST--</option>
<option id="blastn"  value="blastn"> BLASTn (Search Nucleotide Databases Using a Nucleotide Query) </option>
<option id="blastp" value="blastp"> BLASTp (Search Protein Databases Using a Protein Query) </option>
<option id="tblastn" value="tblastn"> tBLASTn (Search Translated Nucleotide Databases Using a Protein Query) </option>
<option id="blastx" value="blastx"> BLASTx (Search Protein Databases Using a Translated Nucleotide Query)</option> 
<option id="tblastx" value="tblastx"> tBLASTx (Search Translated Nucleotide Databases Using a Translated Nucleotide Query) </option>
</select>
</div>
</td></tr>
</table>
<p class="shade"> (Make sure you select an appropriate type of BLAST depending on the type of query inserted previously) </p>
</fieldset>
<fieldset><legend><strong> Select Target Database </strong></legend>
<table><tr><td>
<?php

if (!isset($_SESSION['user'])) {
	$access = 'guest';
}
elseif ($_SESSION['user'] == 'admin')
 {
	 $access = 'admin';
 }
 else
 {
	 $access = 'guest';
 }

 include 'get_blast_db.php';

 $header_values = get_blast_db($access);

 echo("<select name=\"target\">");
 echo("<option value=\"Select\">--Select a database--</option>");
 foreach($header_values as $value)
 {
     echo("<option value=\"$value\"> $value </option>");
 }
 echo("</select></td></tr></table>
<p class=\"shade\"> (Make sure you select an appropriate type of database (nucleotide or protein), depending on the type of BLAST selected previously) </p>");
?>
</fieldset>
<fieldset><legend><strong> Filtering parameters </strong></legend>
<table>
<tr><td> Output file format 
</td>
<td>
<select name="format">
<option value="666"> Pairwise + HTML Alignment </option>
<option value="0"> Pairwise </option>
<option value="7"> Tabular </option>
<option value="777"> Tabular + Abundance Analysis </option>
<option value="10"> Comma-Separated Values </option>
</select>
</td><td></td></tr>
<tr><td> Expect threshold </td>
<td><input type="text" size=5 name=evalue value="0.01" /></td><td><p class="shade"> (Statistical significance of the result) </p></td></tr>
<tr><td> Word Size </td>
<td><input type="text" size=6 name=word_size value="default"/></td><td><p class="shade"> (Seed word size. Default is 11 for nucleotide and 3 for protein) </p>
</td></tr></table>
</fieldset>
<fieldset><legend><strong> User information </strong></legend>
<table>
<tr><td> Job title </td>
<td><input type="text" size=25 name=jobtitle /></td><td> <p class="shade"> (Optional) </p></td></tr>
</table>
</fieldset>
<p><input type="submit" value="Submit BLAST!"></p>
</div>
</form>
<div class=list>
<table class="right"><tr><td>
<a href="tutorial.php#example1" title="Help">Help <img style="border:0" src="images/information.gif" alt="help icon"></a>
<a href="blast_form_filled.php" title="Sample Form">Sample Data <img style="border:0" src="images/sampledata.gif" alt="sample icon"></a>
</td></tr></table>
</div>
<?php include "footer.html"?>
</div> <!-- body -->
</body>
</html>
