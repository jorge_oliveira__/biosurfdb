<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link href="./css/default.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="css/navbar.css">
<script src="jquery-1.11.1.min.js"></script>
<script src="navbar.js"></script>
<script type="text/javascript">
$(document).ready(function() {
                // Animate loader off screen
                 $(".se-pre-con").fadeOut("slow");;
                                         });
</script>
<script>
function copyVal() {
var c = document.getElementById(\'copyArea\');
c.value = document.getElementById(\'copyButton\').value;
r=document.getElementById(\'copyArea\').createTextRange();
r.select();
r.execCommand(\'copy\');
}
</script>
<title>BioSurfDB</title>
</head>
<body>
<div class="se-pre-con"></div>
<div class="body">
<?php include "header_and_left_bar.php"?>
<p>&nbsp;</p><p></p><div class="title">Pathway Graph</div><p></p>
<div class="list">
<FORM><INPUT Type="button" VALUE="Back to previous page" onClick="history.go(-1);return true;"></FORM>
<?php

$stamp= time();

if (!isset($_POST['data'])){ echo "<div class=\"error\">Error: Please select one or more entries from the table</div>"; die;}

foreach($_POST['data'] as $line)
{

	//$data = $data . "\n" . $value;
	
	$aux = explode("::",$line);

	$line = "\"$aux[1] $aux[2]\" -> \"$aux[3]\" ;\n\"$aux[1] $aux[2]\" [fillcolor=\"yellow\", style=\"rounded,filled\", shape=\"diamond\"] ;\n\"$aux[3]\" [ fontcolor=\"white\", color=\"darkgreen\", style=\"filled\" shape=\"cds\"];\n\"$aux[3]\" -> \"$aux[5]\";\n\"$aux[5]\" [ style=\"dashed\" , color=\"red\",  fontcolor=\"red\"];\n\"$aux[5]\" -> \"$aux[7]\" ;\n\"$aux[7]\" [ shape=\"box\", color=\"blue\", fontcolor=\"white\",style=\"filled,rounded\" ];\n";


	$data .= $line;

}

$workdir = "/kdbio/home/biosurfdb/public_html/tmp/";

chdir("$workdir");

$dot_file = "$stamp.dot";

$out_file = "$stamp.png";

$handle = fopen($dot_file, 'w');

fwrite($handle, "digraph {\n");

fwrite($handle, $data);

fwrite($handle, "}");

fclose($handle);

$dot_exec = "/kdbio/home/biosurfdb/bin/dot_static";

$dot = $dot_exec." -Tpng $dot_file -o $out_file 2> dot.error";

system($dot);

//unlink("$stamp.fasta");
//unlink("$stamp.data");

echo"<iframe src=\"./tmp/$out_file\" width=\"99.3%\" height=\"380\"></iframe>";

echo("<form action=\"download_blast.php\">
	<input type=\"hidden\" name=\"dir\" value=\"$workdir\" />
	<input type=\"hidden\" name=\"filename\" value=\"./$out_file\" />
                <input type=\"submit\" formaction=\"download_blast.php\" value=\"Download Image\" /></form>");

?>

<?php include "footer.html"?>
</div>
</body>
</html>
