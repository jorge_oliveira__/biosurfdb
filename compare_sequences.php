<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link href="./css/default.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="css/navbar.css">
<script src="jquery-1.11.1.min.js"></script>
<script src="navbar.js"></script>
<script type="text/javascript">
$(document).ready(function() {
		// Animate loader off screen
 		$(".se-pre-con").fadeOut("slow");;
 			});
</script>
<title>BioSurfDB</title>
</head>
<body>
<div class="se-pre-con"><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p>Computing Alignment...<br>Please wait...</div>
<div class="body">
<?php include "header_and_left_bar.php"?>
<p>&nbsp;</p><p></p><div class="title">Comparing Sequences with Clustal and Mview</div><p></p>
<div class="list">
<FORM><INPUT Type="button" VALUE="Back to previous page" onClick="history.go(-1);return true;"></FORM>
<?php

if (!isset($_POST['data'])){ echo "<div class=\"error\">Error: Please select one or more entries from the table</div>"; die;}

$table = $_POST['compare'];
$type = $_POST['algorithm'];

if ($table === 'gene'){ $field = 'gene_name' ; $column_name = 'gene_ncbi'; $blast_type = 'blastn' ; $blast_db = 'nucl' ;}
elseif ($table === 'protein'){ $field = 'protein_name' ; $column_name = 'protein_ncbi'; $blast_type = 'blastp' ; $blast_db = 'prot' ;}

include "get_data.php";

$i=1;
foreach($_POST['data'] as $row)
{
	$aux=explode("::",$row);

	if ($table === 'gene' ) { $value = $aux[4]; $desc='Nucleic';}
	elseif ($table === 'protein' ) { $value = $aux[6]; $desc='Proteic';}
	
	if (isset($gene_ids)) {
		if (in_array("$value", $gene_ids)) { 
			$i++; 
			echo "<div class=\"error\">Warning: You have selected the $table $value more than once.</div>"; 
			continue;
		}
	}

	$gene_ids[] = $value;

	$query = "SELECT $field, sequence from $table where $column_name='$value';";

	$result=get_data($query);

	$result = explode("@",$result);

	$fasta = ">".$result[1]."_".$value."\n".$result[2]."\n";

	$sequence[$value] = $result[2];

	if ( (is_null($result[2])) or ($result[2] ===''))
	{
		echo "<div class=\"error\">Warning: Could not find sequence for $table $value.</div>";
	}
	else
	{
		$data= $data . "\n" . $fasta;
	}

$i++;
}

if ($type === 'fast')
{

foreach($sequence as $id1 => $seq1)
{
	$seq1 = preg_replace("/[^A-Za-z]/", '', $seq1);
	foreach($sequence as $id2 => $seq2)
	{
		$seq2 = preg_replace("/[^A-Za-z]/", '', $seq2);
		if ( ($seq1 === $seq2) and ($id1 !== $id2) and (!in_array("$id1 => $id2", $done)) and (!is_null($seq1)) and ($seq1 !=='') )
		{
			$perfect_matches = $perfect_matches . "<p>$desc sequences $id1 and $id2 are identical.</p><p>$id1: $seq1</p><p>$id2: $seq2</p>";
			$done[] =  "$id1 => $id2";
			$done[] =  "$id2 => $id1";	
		}
	}
}
echo "<p></p>
	        <div class=\"subtitle\">Identical Sequences Analysis:</div>";

if (isset($perfect_matches))
{
	  echo"$perfect_matches</div>";
}
else
{
	  echo"<p>No identical sequences where found!</div>";
}
}
elseif ($type === 'slow')
{

$data = str_replace(' ', '_', $data);

$stamp =  time();

$fasta_file = $stamp . ".fasta";

$workdir = "/kdbio/home/biosurfdb/public_html/tmp/";

chdir("$workdir");

$handle = fopen($fasta_file, 'w');

fwrite($handle, $data);

fclose($handle);

$clustal_exec = "/kdbio/home/jso/microEgo/sw/clustalo";

$clustal = $clustal_exec ." --in /kdbio/home/biosurfdb/public_html/tmp/$fasta_file --outfmt=clu --threads=16  --output-order=tree-order > $stamp.data 2> error.clustal";

//$clustal = $clustal_exec ." --in /kdbio/home/jso/public_html/surfactants/tmp/$fasta_file -- full --distmat-out=$stamp.data --outfmt=clu --threads=32 > $stamp.data 2> error.clustal";

if ($table === 'gene'){ $color = 'identity -dna' ; }
else { $color = 'any' ; }

$mview= "/kdbio/home/jso/microEgo/sw/mview-1.56/bin/mview -in clustal -ruler on -html full -css on -coloring $color $stamp.data > $stamp.html";

$handle = fopen("$stamp.sh", 'w');

fwrite($handle, "$clustal\n");

fwrite($handle, "$mview\n");

fclose($handle);

system($clustal);

system($mview);
unlink("$stamp.fasta");
unlink("$stamp.data");
unlink("$stamp.sh");

echo"<iframe src=\"./tmp/$stamp.html\" width=\"99.3%\" height=\"380\"></iframe>";

}

?>
<?php include "footer.html"?>
</div>
</body>
</html>
